<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Http\Controllers\Controller;
use Input;
use Hash,
    Crypt,
    Session;

class UserController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index() {
        return view('home');
    }

    public function registerUser() {

        $User = new User();
        $User->name = Input::get('name');
        $User->email = Input::get('email');
        $User->password = Hash::make(Input::get('password'));
        $User->save();
        return redirect('/home');
    }

    public function updateProfile() {
        $User = User::where('id', Session::get('userId'))->first();
        $User->name = Input::get('name');
        $User->email = Input::get('email');
        if (Input::get('password')) {
            $User->password = Hash::make(Input::get('password'));
        }
        $User->update();
        Session::flash('updateMessage', 'Profile Updated Successfully!');
        return redirect('/manageProfile');
    }

    public function manageProfile() {
        $data = User::where('id', Session::get('userId'))->first();
        return view('profile.manageProfile', ['data' => $data]);
    }

}
