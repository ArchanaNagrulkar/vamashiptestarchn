<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Addressbook;
use App\Http\Controllers\Controller;
use Input;
use Hash,
    Crypt,
    Session,
    Request;

class AddressController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------
      |
      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index() {
        return view('home');
    }

    public function manageAddressbook() {
        $id = Request::segment(2);
        $data = Addressbook::where('id', $id)->first();

        return view('address.manageAddressbook', ['data' => $data]);
    }

    public function listAddressbook() {
        $data = Addressbook::where('user_id', Session::get('userId'))->get();
        return view('address.addressbookListing', ['data' => $data]);
    }

    public function saveAddress() {
        $AddressBook = new Addressbook();
        foreach (Input::get() as $k => $v) {
            $AddressBook->$k = $v;
        }
        $AddressBook->save();
        Session::flash('saveMessage', 'Address is successfully recorded!!');
        return redirect('/listAddressbook');
    }

    public function deleteAddress($id) {
        Addressbook::where('id', $id)->delete();
        Session::flash('saveMessage', 'One record is deleted successfuly!!');
        return redirect('/listAddressbook');
    }

}
