<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Addressbook;
use Hash;
use Auth,
    DB,
    Crypt;

class APIAddressController extends Controller {

    public function __construct(Request $request) {
         $this->middleware('auth.token');
    }

    public function AddressListing(Request $request, Response $response) {
        $user = Auth::user();
        if ($user) {
            $Address = Addressbook::where('user_id', $user->id)->get()->toJson();
            $userToken = Crypt::encrypt($user->id);
            return response($Address, 200)->header('Content-Type', 'application/json');
        } else {
            return (new Response('Invalid User!', 401))->header('Content-Type', 'application/json');
        }
    }

    public function saveAddress(Request $request, Response $response) {
        $data = $request->all();
        $Addressbook = new Addressbook();
        if ($data) {
            foreach ($data as $k => $v) {
                $Addressbook->$k = $v;
            }
            $Addressbook->save();
            return response()->header('Content-Type', 'application/json');
        }
    }

}
