<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

Route::get('/', 'WelcomeController@index');

Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);
//login and register
Route::post('/userLogin', array('as' => 'userLogin', 'uses' => 'Auth\AuthController@userLogin'));
Route::post('/registerUser', array('as' => 'registerUser', 'uses' => 'Admin\UserController@registerUser'));
//manage Profile
Route::post('/updateProfile', array('as' => 'updateProfile', 'uses' => 'Admin\UserController@updateProfile'));
Route::get('/manageProfile', array('as' => 'manageProfile', 'uses' => 'Admin\UserController@manageProfile'));
//manage addressbook
Route::get('/manageAddressbook/{id?}', array('as' => 'manageAddressbook', 'uses' => 'Admin\AddressController@manageAddressbook'));
Route::get('/listAddressbook', array('as' => 'listAddressbook', 'uses' => 'Admin\AddressController@listAddressbook'));
Route::post('/saveAddress', array('as' => 'saveAddress', 'uses' => 'Admin\AddressController@saveAddress'));
Route::get('/deleteAddress/{id?}', array('as' => 'deleteAddress', 'uses' => 'Admin\AddressController@deleteAddress'));

// API Urls
Route::post('/api/v1/auth', array('as' => 'auth', 'uses' => 'API\APIAuthController@userAuthentication'));
Route::get('/api/v1/addressListing', array('as' => 'AddressListing', 'uses' => 'API\APIAddressController@AddressListing'));
Route::post('/api/v1/saveAddressApi', array('as' => 'saveAddressApi', 'uses' => 'API\APIAddressController@saveAddressApi'));