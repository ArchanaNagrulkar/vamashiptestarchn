<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressbookTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('addressbook', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('title');
                        $table->integer('user_id')->unsigned();
                        $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
			$table->string('contact_name');
			$table->string('address1');
			$table->string('address2');
			$table->string('address3');
			$table->string('city');
			$table->string('state');
			$table->string('pincode');
			$table->string('country');
			$table->string('contact_number');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
