<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\AddressBook;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //Model::unguard();
        //$this->call('UserTableSeeder');
        $Adressbook = new AddressBook();
        $Adressbook->title = "Home Address";
        $Adressbook->contact_name = "Archana Nagrulkar";
        $Adressbook->contact_number = 785963241;
        $Adressbook->address1 = 'Hill Road Bandra';
        $Adressbook->address2 = 'Lane 2';
        $Adressbook->address3 = 'Mumbai';
        $Adressbook->city = 'Mumbai';
        $Adressbook->state = 'Maharshtra';
        $Adressbook->pincode = '421004';
        $Adressbook->country = 'India';
        $Adressbook->save();
    }

}
