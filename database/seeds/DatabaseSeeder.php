<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class DatabaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        //Model::unguard();
        //$this->call('UserTableSeeder');
        $user = new User();
        $user->email = "archu@xyz.com";
        $user->name = "Archana Nagrulkar";
        $user->password = Hash::make('123456789');
        $user->save();
    }

}
