@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Manage Addressbook</div>

                <div class="panel-body">
                    @if(Session::get('updateMessage'))
                    <div class="alert alert-success">
                        {{Session::get('updateMessage')}}
                    </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ URL::route('saveAddress')}}">


                        <div class="form-group">
                            <label class="col-md-4 control-label">Title</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="title" value="{{@$data->title}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Contact Person Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="contact_name" value="{{@$data->contact_name}}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Contact Person Number</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="contact_number" value='{{@$data->contact_number}}'>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Address Line 1</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="address1" value='{{@$data->address1}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Address Line 2</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="address2" value='{{@$data->address2}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Address Line 3</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="address3" value='{{@$data->address3}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Pincode</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="pincode" value='{{@$data->pincode}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">City</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="city"  value='{{@$data->city}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">State</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="state" value='{{@$data->state}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Country</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="country" value='{{@$data->country}}'>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <input type="hidden" class="form-control" name="user_id" value='{{Session::get('userId')}}'>
                                <input type="hidden" class="form-control" name="id" value='{{@$data->id}}'>
                                <button type="submit" class="btn btn-primary">
                                    Submit
                                </button>
                                <a href='{{URL::route('listAddressbook')}}' class="btn btn-primary">
                                    Cancel
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
