@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">AddressBooks

                </div>

                <div class="panel-body">
                    @if(Session::get('saveMessage'))
                    <div class="alert alert-success">
                        {{Session::get('saveMessage')}}
                    </div>
                    @endif

                    <div class='row'>
                        <div class='pull-right'>
                            <a href='{{URL::route('manageAddressbook')}}' class='form-control btn btn-default  btn-info '>Add Addess</a>
                        </div>

                    </div>
                    <div class="row">
                        <table class="table table-bordered table-striped Ntable" >
                            <tr>
                                <th>Title</th>
                                <th>Contact Person Name</th>
                                <th>Contact Person Number</th>
                                <th>Action</th>
                            </tr>
                            <?php
                            if (count($data) != 0) {
                                foreach ($data as $k => $v) {
                                    ?>
                                    <tr>
                                        <td>{{@$v->title}}</td>
                                        <td>{{@$v->contact_name}}</td>
                                        <td>{{@$v->contact_number}}</td>
                                        <td>

                                            <a href="{{URL::route('manageAddressbook')}}/{{$v->id}}" class="btn btn-success btn-sm " title='Edit' ><i class="fa fa-pencil-square-o"></i>
                                            </a>
                                            <a href="{{URL::route('deleteAddress')}}/{{$v->id}}" class="btn btn-danger btn-sm" title='Delete'>Delete
</a>
                                        </td>
                                    </tr>

                                    <?php
                                }
                            } else {
                                ?>
                                <tr><td colspan='4'>No records found!</td></tr>
                            <?php }
                            ?>                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
