@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Home</div>

                <div class="panel-body">
                   

                    <div class="row">
                        <div class="col-md-4">
                            <a href="{{URL::route('manageProfile')}}" class="btn btn-default btn-info form-control">Manage Profile</a>
                        </div>

                        <div class="col-md-4">
                            <a href="{{URL::route('listAddressbook')}}" class="btn btn-default btn-info form-control">Manage Addressbook</a>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
